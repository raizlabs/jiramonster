#import <UIKit/UIKit.h>

#import "NSFetchedResultsController+RZVinylRecord.h"
#import "NSFetchRequest+RZVinylRecord.h"
#import "NSManagedObject+RZVinylRecord.h"
#import "NSManagedObject+RZVinylUtils.h"
#import "NSManagedObjectContext+RZVinylSave.h"
#import "RZCoreDataStack.h"
#import "RZVCompatibility.h"
#import "RZVinyl.h"
#import "NSManagedObject+RZImport.h"
#import "NSManagedObject+RZImportableSubclass.h"
#import "NSManagedObjectContext+RZImport.h"

FOUNDATION_EXPORT double RZVinylVersionNumber;
FOUNDATION_EXPORT const unsigned char RZVinylVersionString[];

