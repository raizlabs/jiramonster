//
//  JiraTests.swift
//  JiraTests
//
//  Created by Brian King on 1/5/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

import XCTest
import RZVinyl
import Alamofire
@testable import Jira

let JSON = ""
class JiraTests: XCTestCase {

    override func setUp() {
        super.setUp()
        JiraCoreDataStack.configureStack()
    }

    private func readJsonFile(file: String) -> AnyObject {
        let filePath = (NSBundle(forClass: object_getClass(self)).resourcePath! as NSString).stringByAppendingPathComponent(file)
        let jsonString = try! String(contentsOfFile: filePath)
        let jsonData = jsonString.dataUsingEncoding(NSUTF8StringEncoding)!
        return try! NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers)
    }
    func testQuery() {
        let context = JiraCoreDataStack.defaultStack().mainManagedObjectContext
        let projects: [Project] = context.query()
    }

    func testProject() {
        guard let json = readJsonFile("Projects.json") as? [[String: NSObject]] else {
            XCTFail("Unable to load JSON")
            return
        }
        let projects = Project.rzi_objectsFromArray(json)
        let project = projects[0]

        XCTAssertEqual(project.name, "Invaluable Shared")
        XCTAssertEqual(project.key, "INVS")

        let originalObjectIDs = projects.map() { $0.objectID }
        let updatedProjectIDs = Project.rzi_objectsFromArray(json).map() { $0.objectID }

        zip(originalObjectIDs, updatedProjectIDs).forEach {
            XCTAssertEqual($0, $1)
        }
    }

    func testIssue() {
        guard let json = readJsonFile("Issues.json") as? [String: NSObject] else {
            XCTFail("Unable to load JSON")
            return
        }
        let pagination = Pagination(contentKey: "issues", contentType: Issue.self)
        JiraCoreDataStack.defaultStack().mainManagedObjectContext.rzi_performImport() {
            pagination.rzi_importValuesFromDict(json)
        }

        XCTAssert(pagination.startAt == 0)
        XCTAssert(pagination.maxResults == 50)
        XCTAssert(pagination.total == 1)
        XCTAssert(pagination.content != nil)

        guard let issue = pagination.content?.first as? Issue else {
            XCTFail("Unable to find issue")
            return
        }
        XCTAssert(issue.key == "RIG-1")
        XCTAssert(issue.summary == "Review User Testing videos")
        XCTAssert(issue.id == 32460)
        XCTAssert(issue.issueDescription == "The task has a description")
        XCTAssertNotNil(issue.issueType)
        XCTAssertNotNil(issue.project)
        XCTAssertNotNil(issue.assignee)
        XCTAssertNotNil(issue.reporter)
        XCTAssertNotNil(issue.creator)
        XCTAssertEqual(issue.creator, issue.reporter)
        XCTAssertEqual(issue.reporter, issue.assignee)
    }
}
