//: Playground - noun: a place where people can play

import UIKit
import XCPlayground
import Jira
import Alamofire

XCPlaygroundPage.currentPage.needsIndefiniteExecution = true
JiraCoreDataStack.configureMemoryStack()

let manager = Alamofire.Manager(username: "", password: "")
let request = manager.request(Jira.AllProjects)

request.responseForRZImport { (r: Response<[Project], NSError>) -> Void in
    switch r.result {
    case .Success(let projects):
        print("P=\(projects)")
    default:
        break
    }
}

debugPrint(request)
