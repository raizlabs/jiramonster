//
//  Request+Endpoint.swift
//  JiraMonster
//
//  Created by Brian King on 1/5/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

import Alamofire
import RZVinyl

/**
 The `APIEndpoint` protocol encapsulates the high-level details necessary to
 communicate with an API endpoint.
 */
public protocol APIEndpoint {

    /// The base URL.
    var baseURLString: String { get }

    /// The encoding to use (e.g. "application/json") for any request parameters.
    var encoding: Alamofire.ParameterEncoding { get }

    /// The HTTP method to use (e.g. "GET").
    var method: Alamofire.Method { get }

    /// The endpoint's path, relative to the base URL.
    var path: String { get }
    
}

extension Manager {
    public func request(endpoint: APIEndpoint, parameters: [String : AnyObject]? = nil, headers: [String : String]? = nil) -> Request {
        let urlstring = endpoint.baseURLString + endpoint.path
        return self.request(endpoint.method, urlstring, parameters: parameters, encoding: endpoint.encoding, headers: headers)
    }
}
