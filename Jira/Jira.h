//
//  Jira.h
//  Jira
//
//  Created by Brian King on 1/6/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Jira.
FOUNDATION_EXPORT double JiraVersionNumber;

//! Project version string for Jira.
FOUNDATION_EXPORT const unsigned char JiraVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Jira/PublicHeader.h>

