//
//  JiraEndpoint.swift
//  JiraMonster
//
//  Created by Brian King on 1/5/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

import Alamofire

public enum Jira {
    case AllProjects
    case IssuesForProject(projectKey: String)
    case CommentsForIssue(issueKey: String)
}

extension Jira : APIEndpoint {

    public var baseURLString: String {
        return "https://raizlabs.atlassian.net/rest/api/2"
    }

    public var encoding: Alamofire.ParameterEncoding {
        return .URL
    }

    public var method: Alamofire.Method {
        return .GET
    }

    public var path: String {
        switch self {
        case .AllProjects:
            return "/project"
        case .IssuesForProject(let projectKey):
            return "/search?jql=project=\(projectKey)"
        case .CommentsForIssue(let issueKey):
            return "/issue/\(issueKey)/comment"
        }
    }

}
