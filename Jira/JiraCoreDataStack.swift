//
//  JiraCoreDataStack.swift
//  JiraMonster
//
//  Created by Brian King on 1/12/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

import Foundation
import RZVinyl

public class JiraCoreDataStack : RZCoreDataStack {
    public class func configureStack() {
        let model = NSManagedObjectModel.mergedModelFromBundles([NSBundle(forClass: self)])
        let stack = JiraCoreDataStack(
            model: model!,
            storeType: NSSQLiteStoreType,
            storeURL: nil,
            persistentStoreCoordinator: nil,
            options: RZCoreDataStackOptions(rawValue: 0))!
        RZCoreDataStack.setDefaultStack(stack)
    }

    public class func configureMemoryStack() {
        let model = NSManagedObjectModel.mergedModelFromBundles([NSBundle(forClass: self)])
        let stack = JiraCoreDataStack(
            model: model!,
            storeType: NSInMemoryStoreType,
            storeURL: nil,
            persistentStoreCoordinator: nil,
            options: RZCoreDataStackOptions(rawValue: 0))!
        RZCoreDataStack.setDefaultStack(stack)
    }
}