//
//  Manager+JIRA.swift
//  JiraMonster
//
//  Created by Brian King on 1/12/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

import Alamofire

extension Manager {

    public convenience init(username: String, password: String) {
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        var headers = Manager.defaultHTTPHeaders
        headers["Accept"] = "application/json"
        headers["Content-Type"] = "application/json"

        let string = username + ":" + password + ""
        let data = string.dataUsingEncoding(NSUTF8StringEncoding)
        if let base64 = data?.base64EncodedDataWithOptions([]),
            let authHeader = String(data: base64, encoding: NSUTF8StringEncoding) {
                print("\(authHeader)")
                headers["Authorization"] = "Basic ZXJpYy5zbG9zc2VyOktpbm5nNDJLaW5uZw=="
        }

        configuration.HTTPAdditionalHeaders = headers

        self.init(configuration: configuration)
    }
    
}