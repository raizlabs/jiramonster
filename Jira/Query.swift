//
//  Query.swift
//  JiraMonster
//
//  Created by Brian King on 1/13/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

import CoreData

public typealias NSFetchRequestConfigurationBlock = (NSFetchRequest) -> Void
public let NSFetchRequestConfigurationBlockDefault: NSFetchRequestConfigurationBlock = {fr in }

public extension NSManagedObjectContext {
    public func query<T: NSManagedObject>(
        predicate: NSPredicate? = nil,
        sortDescriptors: [NSSortDescriptor]? = nil,
        configuration: NSFetchRequestConfigurationBlock = NSFetchRequestConfigurationBlockDefault
        ) -> [T]
    {
        let request = NSFetchRequest(entityName: T.rzv_entityName())
        request.predicate = predicate
        request.sortDescriptors = sortDescriptors
        configuration(request)
        return self.safeExecuteFetchRequest(request)
    }

    public func safeExecuteFetchRequest<T: NSManagedObject>(request: NSFetchRequest) -> [T] {
        do {
            return try executeFetchRequest(request) as! [T]
        }
        catch let e as NSError {
            print("Error performing fetch: \(e)")
            return []
        }
    }
}

