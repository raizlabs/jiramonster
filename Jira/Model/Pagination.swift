//
//  Pagination.swift
//  JiraMonster
//
//  Created by Brian King on 1/13/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

import Foundation
import RZImport

let j = NSObject.self

public class Pagination : NSObject {

    public var expand: [String] = []
    public var startAt: Int = 0
    public var maxResults: Int = 50
    public var total: Int = 0

    public var contentKey: String
    public var content: [AnyObject]?
    public var contentType: NSObject.Type

    public init(contentKey: String, contentType: NSObject.Type) {
        self.contentKey = contentKey
        self.contentType = contentType
        super.init()
    }

}

extension Pagination : RZImportable {
    public func rzi_shouldImportValue(value: AnyObject, forKey key: String) -> Bool {
        if key == self.contentKey {
            guard let json = value as? [[String:NSObject]] else {
                fatalError("Invalid JSON Response Type")
            }
            self.content = self.contentType.rzi_objectsFromArray(json)
            return false
        }
        else if key == "expand" {
            guard let json = value as? String else {
                fatalError("Invalid JSON Response Type")
            }

            self.expand = json.componentsSeparatedByString(",")
            return false
        }
        return true
    }
}