//
//  Field.swift
//  JiraMonster
//
//  Created by Brian King on 1/12/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

import Foundation
import CoreData


final public class Field: NSManagedObject {

    @NSManaged var custom: Bool
    @NSManaged var navigable: Bool
    @NSManaged var orderable: Bool
    @NSManaged var searchable: Bool

    @NSManaged var id: String
    @NSManaged var name: String
    @NSManaged var schemaType: String

}
