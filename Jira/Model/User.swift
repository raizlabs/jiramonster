//
//  User.swift
//  JiraMonster
//
//  Created by Brian King on 1/12/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

import Foundation
import CoreData


final public class User: NSManagedObject {

    @NSManaged var jiraURLString: String
    @NSManaged var name: String
    @NSManaged var displayName: String
    @NSManaged var key: String
    @NSManaged var emailAddress: String
    @NSManaged var timeZone: String

    @NSManaged var attachments: Set<Attachment>
    @NSManaged var avatarURLs: [String:String]

    override public class func rzv_primaryKey() -> String {
        return "jiraURLString"
    }

    override public class func rzv_externalPrimaryKey() -> String {
        return "self"
    }

    override public class func rzi_customMappings() -> [String : String] {
        return ["self":"jiraURLString"]
    }

}
