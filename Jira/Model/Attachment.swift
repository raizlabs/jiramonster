//
//  Attachment.swift
//  JiraMonster
//
//  Created by Brian King on 1/12/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

import Foundation
import CoreData


final public class Attachment: NSManagedObject {

    @NSManaged var jiraURLString: String
    @NSManaged var name: String

    @NSManaged var content: String
    @NSManaged var thumbnail: String

    @NSManaged var mimeType: String
    @NSManaged var size: Int64

    @NSManaged var author: User

    override public class func rzv_primaryKey() -> String {
        return "jiraURLString"
    }

    override public class func rzv_externalPrimaryKey() -> String {
        return "self"
    }

    override public class func rzi_customMappings() -> [String : String] {
        return ["self":"jiraURLString"]
    }

}
