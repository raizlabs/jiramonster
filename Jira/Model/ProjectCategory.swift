//
//  ProjectCategory.swift
//  JiraMonster
//
//  Created by Brian King on 1/13/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

import Foundation
import CoreData

@objc(ProjectCategory)
public class ProjectCategory: NSManagedObject {

    @NSManaged var id: Int64
    @NSManaged var jiraURLString: String
    @NSManaged var name: String
    @NSManaged var projectCategoryDescription: String
    @NSManaged var projects: Set<Project>

    override public class func rzv_primaryKey() -> String {
        return "jiraURLString"
    }

    override public class func rzv_externalPrimaryKey() -> String {
        return "self"
    }

    override public class func rzi_customMappings() -> [String : String] {
        return ["description" : "projectCategoryDescription"]
    }
}
