//
//  Project.swift
//  JiraMonster
//
//  Created by Brian King on 1/5/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

import Foundation
import CoreData
import RZVinyl
import RZImport

@objc(Project)
final public class Project: NSManagedObject {

    @NSManaged var id: Int64
    @NSManaged var jiraURLString: String

    @NSManaged var key: String
    @NSManaged var name: String
    @NSManaged var issueTypes: Set<IssueType>
    @NSManaged var projectCategory: ProjectCategory?
    @NSManaged var avatarURLs: [String:String]

    override public class func rzv_primaryKey() -> String {
        return "jiraURLString"
    }

    override public class func rzv_externalPrimaryKey() -> String {
        return "self"
    }

}
