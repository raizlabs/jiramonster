//
//  Defines.swift
//  JiraMonster
//
//  Created by Brian King on 1/11/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

import Foundation

public struct ImageSize {
    public static let XSmall = "8x8"
    public static let Small = "16x16"
    public static let Medium = "32x32"
    public static let Large = "64x64"
}