//
//  Issue.swift
//  JiraMonster
//
//  Created by Brian King on 1/13/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

import Foundation
import RZImport
import RZVinyl


public class Issue: NSManagedObject {

    @NSManaged var id: Int64
    @NSManaged var jiraURLString: String

    @NSManaged var key: String

    @NSManaged var summary: String
    @NSManaged var issueDescription: String
    @NSManaged var issueType: IssueType?
    @NSManaged var project: Project?

    @NSManaged var creator: User?
    @NSManaged var reporter: User?
    @NSManaged var assignee: User?

}

extension Issue {

    override public class func rzv_primaryKey() -> String {
        return "jiraURLString"
    }

    override public class func rzv_externalPrimaryKey() -> String {
        return "self"
    }

    override public class func rzi_customMappings() -> [String : String] {
        return ["description":"issueDescription"]
    }

    override public class func rzi_nestedObjectKeys() -> [String] {
        return ["assignee", "creator", "reporter", "issueType", "project"]
    }

    public override func rzi_shouldImportValue(value: AnyObject, forKey key: String) -> Bool {
        if key == "fields" {
            guard let fields = value as? [String:NSObject] else {
                print("Unexpected type in fields key")
                return false
            }
            self.rzi_importValuesFromDict(fields)
            return false
        }
        return true
    }
}