//
//  IssueType.swift
//  JiraMonster
//
//  Created by Brian King on 1/5/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

import Foundation
import CoreData

@objc(IssueType)
final public class IssueType: NSManagedObject {

    @NSManaged var id: Int64
    @NSManaged var jiraURLString: String
    
    @NSManaged var key: String
    @NSManaged var name: String
    @NSManaged var issueTypeDescription: String
    @NSManaged var projects: Set<Project>

    override public class func rzv_primaryKey() -> String {
        return "jiraURLString"
    }

    override public class func rzv_externalPrimaryKey() -> String {
        return "self"
    }

    override public class func rzi_customMappings() -> [String : String] {
        return ["self":"jiraURLString"]
    }

}

