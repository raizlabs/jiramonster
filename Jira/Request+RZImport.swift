//
//  Request+RZImport.swift
//  JiraMonster
//
//  Created by Brian King on 1/12/16.
//  Copyright © 2016 Raizlabs. All rights reserved.
//

import Alamofire
import RZVinyl

extension Request {
    public func responseForRZImport<T: NSManagedObject>(completionHandler: Response<[T], NSError> -> Void) -> Self {
        let responseSerializer = ResponseSerializer<[T], NSError> { request, response, data, error in
            guard error == nil else { return .Failure(error!) }

            let JSONResponseSerializer = Request.JSONResponseSerializer(options: .AllowFragments)
            let result = JSONResponseSerializer.serializeResponse(request, response, data, error)
            var importArray: [Dictionary<String, NSObject>] = []
            switch result {
            case .Success(let value):
                if let arrayValue = value as? [Dictionary<String, NSObject>] {
                    importArray = arrayValue
                }
                else if let dictionary = value as? Dictionary<String, NSObject> {
                    importArray = [dictionary]
                }
                else {
                    return .Failure(Error.errorWithCode(.JSONSerializationFailed, failureReason: "JSON Must be of type Dictionary<String, NSObject> or Array<Dictionary<String, NSObject>>"))
                }
            case .Failure(let error):
                return .Failure(error)
            }
            let stack = RZCoreDataStack.defaultStack()
            let context = stack.backgroundManagedObjectContext()
            var importedObjects: [T] = []
            context.rzi_performImport() {
                importedObjects = T.rzi_objectsFromArray(importArray) as! [T]
            }

            do {
                try context.rzv_saveToStoreAndWait()
                return .Success(importedObjects)
            } catch let error as NSError {
                return .Failure(error)
            }
        }

        return response(responseSerializer: responseSerializer) { response in
            switch response.result {
            case .Success(let backgroundThreadObjects):
                let stack = RZCoreDataStack.defaultStack()

                let mainThreadObjects = backgroundThreadObjects.map() { object in
                    return object.rzv_objectInContext(stack.mainManagedObjectContext)!
                }
                completionHandler(Response(
                    request: response.request,
                    response: response.response,
                    data: response.data,
                    result: .Success(mainThreadObjects)))
            default:
                completionHandler(response)
            }
        }
    }
}
